#ifndef SORTER_H
#define SORTER_H


#include "include.h"
#include "parser.h"
#include "heap.h"

// toggles for different modes

//#define RANDOM

//#define USING_HEAP


inline Node* generateNode()  //used in testing the sorter without parser
{
	static std::string word;
	word.clear();
	for(unsigned int i = 0; i<6; ++i)
		word.push_back(rand()%5+65);  //generate a random word
	return(new Node(word,rand(),rand()));
}


class Sorter
{
	public:
		Sorter();
		Sorter(char *fileName);
		~Sorter();

		void open(char *fileName);
		void process();
		void PrintWords(std::ostream & out);

		friend
        std::ostream& operator << (std::ostream& out, const Sorter & S);

	private:
		void initialize();
		void checkTopTen (Node* checkThis);
		//lots of variables...
		Node* topTen[10];  //top ten used words
		Parser P;   //our parser
		std::vector<Node*> sorted;  //a single list of all Nodes in alphabetical order
		timer iTime, rTime;
		int para, sent, syla, total, totUnique, unique[26];  //counts of stuff
#ifdef USING_HEAP
		heap<Node> box[26];
#else
		binaryTree<Node> box[26];
#endif
};


#endif // SORTER_H
