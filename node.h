#ifndef NODE_H
#define NODE_H

#include "include.h"

struct Node
{
		std::string word;
		std::vector<int> location;

		Node(){}
		~Node(){}
//		Node(Node &takeMine)
//		{
//			word.swap(takeMine.word);
//			location.swap(takeMine.location);
//		}
		Node(Node const &copyMine) : word(copyMine.word), location(copyMine.location)
		{}
		Node(std::string &takeMe, int par, int sen) : word(takeMe)
		{
			//word.swap(takeMe);  //seems like nothing is faster than the copy constructor for words
			location.push_back(par);
			location.push_back(sen);
		}
		void operator += (const Node &addMine)
		{
			for(unsigned int i = 0, top = addMine.location.size() ;i < top ; ++i)  //add all the elemetns in the location vector to ours.
				location.push_back(addMine.location[i]);
		}
		unsigned int first()const
		{
			return (word[0] - 'A');  //get the number of the first - 'A' = 0, 'Z' = 25
		}
		int count()const
		{
			return (location.size()/2); //as even's are paragraphs, odds are sentances
		}
		friend
		std::ostream& operator <<(std::ostream &out, const Node &n)  //this is a mess...
		{
			out<<n.word<<" found at "<<n.location[0];
			for(unsigned int i = 1, top = n.location.size(); i < top; ++i)
			{
				if(!(i%10))
					out<<std::endl;
				if(!(i%2))
					out<<" and ";
				else
					out<<"|";
				out<<n.location[i];
			}
			out<<std::endl;
			return out;
		}
		friend
		std::istream& operator >>(std::istream &in, Node &n)
		{
			n.word.clear();
			n.location.clear();
			in>>n.word;
			return in;    //incomplete but we don't actually use this
		}
		friend
		bool operator == (const Node &l, const Node &r)
		{
			return l.word == r.word;
		}
		friend
		bool operator < (const Node &l, const Node &r)
		{
			return l.word < r.word;
		}
};


#endif // NODE_H
