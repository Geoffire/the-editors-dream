#ifndef HEAP_H
#define HEAP_H

#include "include.h"

enum TYPE {LESS, GREATER};

template<typename T>
class heap
{
    public:
		heap(TYPE m = LESS);
		heap(const heap<T> &)
		{ throw DONT_DO_THAT; }  //don't do it
		void operator = (const heap<T> &)
        { throw DONT_DO_THAT; }
        ~heap();

		heap<T>& operator<<(T *insertMe);
        heap<T>& operator>>(T *&putHere);

		void insert(T *insertMe);
        bool pop(T *&putHere);

        void clear();
        bool empty() const;
		unsigned int count() const;

		const T& peek() const;

        template<typename U>
        friend
        std::ostream& operator<<(std::ostream& out, heap<U> &h);
        template<typename U>
        friend
        std::istream& operator>>(std::istream& in, heap<U> &h);

    private:
        typedef bool (heap<T>::*fptr)(unsigned int x, unsigned int y) const;

		std::vector<T*> box;  //the only variable if you don't count the function pointer
        fptr compare;

        void reheapifyUp();
        void reheapifyDown();

        bool greater(unsigned int x, unsigned int y) const;
        bool less(unsigned int x, unsigned int y) const;
        //general helpers
        void swap(unsigned int d, unsigned int s);
        bool is(unsigned int e)const;
        T*& get(unsigned int e);
        const T* get(unsigned int e)const;

};

template<typename T>
inline
heap<T>::heap(TYPE m)
{
	if(m==LESS)
		compare = &heap<T>::less;   //single function pointer for compare
    else
        compare = &heap<T>::greater;
}

template<typename T>
heap<T>::~heap()
{
	for(unsigned int i = 0, top = box.size(); i < top; ++i) //delete each node in the vector
    {
        if(T*& hold = box[i])
            delete hold;
    }
}

template<typename T>
inline
heap<T>& heap<T>::operator<<(T *insertMe)
{
    insert(insertMe);
    return *this;
}

template<typename T>
inline
heap<T>& heap<T>::operator>>(T *&putHere)
{
    pop(putHere);
    return *this;
}


template<typename T>
inline
void heap<T>::insert(T *insertMe)
{
	box.push_back(insertMe);
    reheapifyUp();
}


template<typename T>
inline
bool heap<T>::pop(T *&putHere)
{
	unsigned int last = box.size();
	if(!last)
        return false;
	putHere = get(0);  //grab the first T and give it to putHere
	swap(0,--last);    //swap the first and last
    box.pop_back();   //remove the last(was the first)
    reheapifyDown();  //reheapify
    return true;
}


template<typename T>
inline
bool heap<T>::empty() const
{
    return !(bool)(box.size()-1);
}

template<typename T>
inline
const T& heap<T>::peek() const
{
    if(empty())
        throw EMPTY;
	return *get(0);
}

template<typename T>
inline
void heap<T>::clear()
{
    box.clear();
}

template<typename T>
inline
unsigned int heap<T>::count() const
{
    return box.size();
}

template<typename T>
inline
void heap<T>::reheapifyUp()
{
    unsigned int c = box.size()-1,
                 p = up(c);
    while(c != 0)
    {
		if((this->*compare)(c,p))  //if the child is greater than the parent
			swap(p, c);   //swap
        else
			return;  //else return
		c = p;
        p = up(c);
    }
}

template<typename T>
inline
void heap<T>::reheapifyDown()
{
    unsigned int size = box.size();
	if(size<2)  //if only one data or less no point
        return;
    unsigned int loc = 0, candidate;
    while(1) //loop until return reached
    {
		if(is(right(loc)))  //if there is a right
        {
			if((this->*compare)(left(loc),right(loc))) //find which child is smallest
				candidate = left(loc);   //select the smallest
            else
                candidate = right(loc);
        }
		else if(is(left(loc)))  //if there is only a left
			candidate = left(loc); //select the smallest
        else
			return;  //no child return

		if((this->*compare)(candidate, loc))  //is the candidate child smaller than the paraent?
        {
			swap(candidate, loc);  //swap it and
			loc = candidate;   //and move that direction
        }
        else
			return;  //else return
    }
}

template<typename T>
bool heap<T>::greater(unsigned int x, unsigned int y) const
{
    return *get(y) < *get(x);
}

template<typename T>
bool heap<T>::less(unsigned int x, unsigned int y) const
{
    return *get(x) < *get(y);
}

//these all same as binaryTree versions

template<typename T>
inline
void heap<T>::swap(unsigned int d, unsigned int s)
{
    if (box.size() < d+1)
        box.resize(d+1);
    T* hold = box[d];
    box[d] = box[s];
    box[s] = hold;
}

template<typename T>
inline
bool heap<T>::is(unsigned int e) const
{
    if(box.size() < e+1)
        return false;
    return static_cast<bool>(box[e]);
}

template<typename T>
inline
T*& heap<T>::get(unsigned int e)
{
    return box[e];
}

template<typename T>
inline
const T* heap<T>::get(unsigned int e) const
{
    return box[e];
}

template<typename U>
std::ostream& operator<<(std::ostream& out, heap<U> &h)
{
    U* hold;
    while(h.pop(hold))
    {
        out<<*hold<<" ";
        delete hold;
    }
    return out;
}

template<typename U>
std::istream& operator>>(std::istream& in, heap<U> &h)
{
    U* hold = new U;
    while(in>>*hold)
    {
        h.insert(hold);
        hold = new U;
    }
    delete hold;
    return in;
}
#endif // HEAP_H

